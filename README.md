# SATIE4Score

A template project and tools to work with [SATIE](https://gitlab.com/sat-metalab/satie) from [OSSIA Score](https://ossia.io/).  

This toolbox contains modular devices, providing simple UIs to minimize interaction with SATIE/SuperCollider.

# SETUP

How to use the template :

 - Clone the whole repository and open the files directly.  

_Or_ 

 - Download [The SATIE Configuration](/S4S-SATIE-Configuration.scd)
 - Download [The Score Template](/S4S-Score-Template.score)
 - Place [The SATIE OSC device](/SATIE%20OSC.device) in `\ossia\score\packages\default\Devices\OSC`
 - Execute the SATIE configuration and run the template's score file
 

# Discover

Download and install all devices by following the [devices installation instructions](Doc/DevicesInstallation.md).

For an introduction to the SATIE4Score workflow, please read the [Get Started](Doc/Get-Started-with-SATIE4Score.md) documentation.

For a list of all the devices contained in the toolbox, see the [List of Devices](Doc/DevicesList.md).

Download [loopMIDI](Doc/loopMIDI-SETUP.md) to use the toolbox with MIDI coming from external applications.

