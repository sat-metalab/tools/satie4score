# SATIE4Score : Get Started

SATIE4Score is a set of [OSSIA Score](https://ossia.io/) tools conceived to minimize interactions with the [SATIE Quark](https://gitlab.com/sat-metalab/SATIE) for [SuperCollider](https://supercollider.github.io/).  

They allow you to control your SATIE Nodes, that is your SynthDefs (or sources), your groups (busses/sends) and processes (patterns).  
You still have to use SuperCollider to create, delete, and manage your Nodes. But once you've done this, every parameter (argument) can be controlled from Score.

For a list of all the tools available in the SATIE4Score Toolbox see the [List of Devices](DevicesList.md).

The following tutorial is meant to be an introduction to the possibilities offered by the Toolbox, a template is provided and can be used as a starting point to further experiment with the tools.

## The Template

The template looks like this :

![Template View](media/Template_Screens/S4S-template-full-ControlSend.png "Template View")

Score allows you to draw cables between outputs and inputs of different devices to connect them.  
There is two types of devices. *Controllers*, and *Senders* (those beginning with "Satie").  
Connect any *Controller* to any *Sender* and start sending data to SATIE.  
The *Senders* have boxes in which you can write the name of your target and the name of the parameter you want to control (except SatieSourceUpdate because it is by default a list of every Spatial Parameter in that order : Azimuth, Elevation, Gain, Delay, LowPass).  

<p>&nbsp;</p> 

Before we start, there is a few line of code to execute in SuperCollider, in order to get SATIE running with a SynthDef and a Reverb that we're going to be playing with.

You can open the "S4S-SATIE-Configuration.scd" file contained in the repository and execute each line to setup SATIE.  

We're good to go.

<p>&nbsp;</p> 

## 1. Retrieve MIDI Data

There are two ways to retrieve MIDI Data with SATIE4Score.  
You can connect the output of a MIDI file (loaded into Score) to the input of midiExploder.

![Retrieve MIDI](media/Template_Screens/RetrieveMIDIFile.png "Retrieve MIDI")

Or you can choose to input an external MIDI port from the inspector view of midiExploder.  
See the [loopMIDI setup](loopMIDI-SETUP.md) for an in-depth review on how to send MIDI data between applications.

![Retrieve MIDI](media/Template_Screens/RetrieveMIDIFileExternal.png "Retrieve MIDI")

Now that we have some MIDI coming through, we can start to play with a SynthDef.  

###### *Note : retrieveing MIDI is not mandatory to use SATIE4Score, some devices like Spatializer can just be used to control spatial parameters of a SynthDef. No MIDI involved.*  

<p>&nbsp;</p>

## 2. Control a SynthDef : MIDI

For this example, we'll choose Marimba, a default SATIE SynthDef.  

The most basic use of a MIDI file is to send musical information to an instrument. The instrument then plays some notes, as described in the MIDI file.  

You can do that with midiExploder : 

![Send MIDI](media/Template_Screens/SendMIDItoSATIE.png "Send MIDI")

Connect the Pitch Output to SatieSourceSet to send direct MIDI notes to the selected SynthDef and the argument controlling the frequency/pitch.  

If the argument controlling the frequency/pitch of your SynthDef takes frequency values in Hertz rather than MIDI notes, you can place the MIDI2Frequency Micromap preset between midiExploder and SatieSourceSet to convert, as shown on the screenshots above.  

MIDI data is just data in the end, so you can use it to control other parameter, particularly with the MIDI2Space device, you can use MIDI to control spatialization *(this is not set by default in the Template, but you can try and do it !)* :  

![MIDI to Azimuth](media/Template_Screens/MIDItoAzi.png "MIDI to Azimuth")

###### *MIDI2Space scales the MIDI value range (0 to 127) to the Azimuth Degrees range (-180 to 180), or the Elevation Degrees range (-90 to 90).*  

With this setup, the note value is converted into the corresponding Azimuth degree, and then transferred to the "aziDeg" parameter of the Marimba SynthDef.

We've seen how to retrieve MIDI and different ways of using it, now let's see what else can we control.

<p>&nbsp;</p>

## 3 Control a SynthDef : Spatialization

You can control a SynthDef's spatialization with your MIDI values, but though it is fun and creative, it is not the most practical way if you want to be precise.  

For that there is the Spatializer device, it works in pair with the SatieSourceUpdate plugin which is specifically sending spatial data to SATIE.

![Spatial Parameters](media/Template_Screens/SpatialParameters.png "Control Spatialization")  

This is pretty straightforward.  
Here, every spatial parameter is transferred from the Spatializer to SatieSourceUpdate.   
SatieSourceUpdate is then sending these values to the Marimba SynthDef in that order : Azimuth, Elevation, Gain, Delay, LowPass.

Again, even if these two devices works well together, SatieSourceUpdate can receive any value (as long as it is within a range that the parameter can receive).  
The same goes for Spatializer, you can try to send its values to another receiver, see what it does.  

*Note : the inputs and outputs of all devices have a name, so when you point your cursor on one, you can see to which parameter it corresponds by looking at the bottom right in the inspector panel.*  

We've seen how to send data like MIDI notes and spatial parameters to a SynthDef.  
Now let's control an effect.  

<p>&nbsp;</p>

## 4 Control an Effect (group) : Reverb  

![Control an Effect](media/Template_Screens/EffectReverbControl.png "Controlling a Reverb")

The Scaler is a useful device, you can enter any positive minimum and maximum value, and it will map the "Any" Dial to these extremes. This way you can write in your own values and control any parameter that you've previously defined, without being restricted in a range of value.  
Here we are using it to control the decay time of a Reverb, we chose a minimal value of 1 second, and a maximal value of 10 seconds.  

SatieNodeSet can adress any type of Node, you can choose between Source, Group and Process.  
Here, our Reverb is instantiated in the group called "defaultFx", and its decay parameter is called "rt60".  

At any time, if you want to control more parameters, for example the size of the reverb, you can load a new Scaler and a new SatieNodeSet, and just write for example "Size" in the "Key Name" box of SatieNodeSet. Then choose your minimal and maximal values to operate within. Repeat this operation for as much parameter as you need.  

<p>&nbsp;</p>

## Conclusion

This is basically how the SATIE4Score Toolbox works.  

Now if you have your own SynthDefs, [register them into SATIE](https://sat-metalab.gitlab.io/satie/en/tuto/plugins.html), load devices in Score, write your SynthDef name and parameters in the corresponding boxes of the *Senders*, connect some *Controllers* to it, and start playing !