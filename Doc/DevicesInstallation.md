# Importing SATIE4Score devices into Score

*Note : Requires [Ossia Score](https://ossia.io/) and [SuperCollider](https://supercollider.github.io/) with the [SATIE Quark](https://gitlab.com/sat-metalab/SATIE). To install, manage and learn these softwares please refer to the links.*

For Windows x64 :

1) The Javascript and Micromap folders must be copied into : ```YourPathTo\ossia\score\packages\user\presets```

2) "SATIE OSC Device" must be copied into : ```YourPathTo\ossia\score\packages\default\Devices\OSC```


