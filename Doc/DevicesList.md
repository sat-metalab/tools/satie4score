# Liste de devices "SATIE4SCORE Toolbox"

This is the list of all the devices contained in the SATIE4Score Toolbox.  
For practicals examples of how these works, please refer to the [Get Started](Get-Started-with-SATIE4Score.md) page.

## MICROMAP PRESETS (4):

 - MIDI2Frequency (micromap) : Converts a MIDI note value (int) into to the corresponding frequency (float).

   - INPUT(1) : MIDI Note Input
   - OUTPUT(1) : Frequency Output 

<p>&nbsp;</p>  

 - Velocity2Decibels (micromap) : Converts a Velocity value (int) into to the corresponding Decibel value (float).

   - INPUT(1) : Velocity Input
   - OUTPUT(1) : Decibel Output 

<p>&nbsp;</p> 

 - MIDIValueToAzimuth (micromap) : Converts a MIDI Note or Velocity value into Azimuth Degrees.

   - INPUT(1) : MIDI Note Input (int: values between 0 and 127)
   - OUTPUT(1) : Azimuth Degree Output (float: values between -180 and 180)

<p>&nbsp;</p>  

 - MIDIValueToElevation (micromap) : Converts a MIDI Note or Velocity value into Elevation Degrees.

   - INPUT(1) : MIDI Note Input (int: values between 0 and 127)
   - OUTPUT(1) : Elevation Degree Output (float: values between -90 and 90)

<p>&nbsp;</p>  

## JAVASCRIPT PRESETS (7):

 - midiExploder (js) : Receives a MIDI message and extracts musical data.

   - INPUT(1) : midi message from internal/external source
   - OUTPUTS(3) :
     - Channel
     - Note
     - Velocity

<p>&nbsp;</p>  

 - Spatializer (js) : The 5 Spatial Parameters of SATIE, outputs float values. Works Primarly with 'SatieSourceUpdate'.

   - Dials(5) : 
      - "Azimuth" (float: min:-180, max: 180)
      - "Elevation" (float: min:-90, max: 90)
      - "Gain" (float: min:-99, max: 0)
      - "Delay" (float: min: 0, max: 1000)
      - "LowPass" (float: min: 5, max: 18000)
   - OUTPUTS(5) :
      - Azimuth Output
      - Elevation Output
      - Gain Output
      - Delay Output
      - LowPass Output

<p>&nbsp;</p>  

 - MIDI2Space (js) : Converts a MIDI note value and/or a velocity value into either Azimuth or Elevation degrees.

   - Enums(4) : 
      - Note Into Azimuth => "Off" or "aziDeg"
      - Note Into Elevation => "Off" or "eleDeg"
      - Velocity Into Azimuth => "Off" or "aziDeg"
      - Velocity  Into Elevation => "Off" or "eleDeg"
   - INPUTS(2) :
      - MIDI Note Input
      - Velocity Input
   - OUTPUTS(4) :
      - Note to Azimuth Output
      - Note to Elevation Output
      - Velocity to Elevation Output
      - Velocity to Elevation Output

<p>&nbsp;</p>  

 - SatieSourceSet (js) : Takes any value and sends it to any parameter of any SynthDef.

   - LineEdit(2) :
      - *SynthDef Name*
      - *Argument Name*
   - Input(1) : Any Value Input
   - Output(1) : Value Monitor Output

<p>&nbsp;</p>

 - SatieSourceUpdate (js) : Controls the spatial parameters of a given SynthDef. Each inputs receives a value and sends it to its corresponding parameter. Works Primarly with Spatializer, but can receive any value.

   - LineEdit(1) :
     - *Enter SynthDef Name*
   - Inputs(5) : 
     - Azimuth Input
     - Elevation Input
     - Gain Input
     - Delay Input
     - LowPass Input
   - Outputs(5) :
     - Azimuth Monitor Output
     - Elevation Monitor Output
     - Gain Monitor Output
     - Delay Monitor Output
     - LowPass Monitor Output

<p>&nbsp;</p>  

 - SatieNodeSet (js) : Takes any value and sends it to any parameter/key of any Node Type.

   - Enum(1) : Node Type => "source", "group" or "process"
   - LineEdit(2) :
      - *Node Name*
      - *Key Name*
   - Input(1) : Any Value Input
   - Output(1) : Value Monitor Output

<p>&nbsp;</p>  

 - Scaler (js) : Scales a range of values between 0 and 1 to any other range between a defined Maximum and a Minimum. Practical to control any parameter of any SynthDef, with SatieSourceSet or SatieNodeSet for example.

   - Enum(1) : On/Off
   - LineEdit(2) :
      - *Enter Min Value*
      - *Enter Max Value*
   - Dial(1) : "Any" (float: min:0, max: 1) 

Notes : 
 - "Monitoring" outputs are used like a debug fonction where you can check if your values are being received well by the device.