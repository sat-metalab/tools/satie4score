# loopMIDI SETUP
## On Windows:
*Download loopMIDI [HERE](https://www.tobias-erichsen.de/software/loopmidi.html)*

loopMIDI allows you to have virtual MIDI ports opened outside of your applications.   
It is useful to send and receive MIDI between different applications.  
Here we are going to use it to send MIDI from Live to Score :

1) Open loopMIDI and set the number of ports you want to open.  
   You can name these ports how you want, and click "+" to create them.

   ![image](media/loopMIDI_screenshots/loopMIDI_window.png "Manage ports inside loopMIDI")

<p>&nbsp;</p> 

2) Open Live, go to Options/Preferences and click the "Link/Tempo/MIDI" tab.  
   In the "MIDI Ports" section at the bottom, you will see every inputs and outputs of the ports that you created.  
   Check their cases depending on if you want to send or receive MIDI messages.

   ![image](media/loopMIDI_screenshots/liveParameters.png "Open ports in Live")

<p>&nbsp;</p>  

3) Now in the I/O section of a MIDI track you want to send outside of Live, open the drop-down menu "No Output" and select on which port you want to send. 

   ![image](media/loopMIDI_screenshots/liveTrackdropdown.png "Select MIDI port on a Live track")

<p>&nbsp;</p>

4) In Score, open the Device Explorer at the bottom left, right click in the empty space and select "Add Device"

   ![image](media/loopMIDI_screenshots/scoreAddDevice.png "Add the loopMIDI device in Score")

<p>&nbsp;</p>

5) Once you've selected "Add Device", a window will open. In the "Protocols" list, look for "MIDI Input", click it and select the Port you would like to use.

   ![image](media/loopMIDI_screenshots/scoreMidiInput.png "Select a port in the loopMIDI device")

<p>&nbsp;</p>

6) *Repeat step 4 and 5 to add more MIDI ports.*

<p>&nbsp;</p>

7) In the Inspector window of the midiExploder device, unfold the "Inputs" drop-down menu and select the port you want to retrieve the MIDI data from.

   ![image](media/loopMIDI_screenshots/scoreInputSelect.png "Select a port inside the midiExploder")

<p>&nbsp;</p>

You can now use the data retrieved from the MIDI message and send it to any other device in Score.  
Read the SATIE Toobox Tutorial to see how to use every SATIE4Score device.